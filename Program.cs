﻿using System.IO;
using System.Text;

using static SAMSharp.Sam;
using static SAMSharp.Reciter;
using System;
using System.Media;

namespace SAMSharp
{
    class Program
    {
        static Stream Wave(byte[] buffer, int bufferlength)
        {
            var strm = new MemoryStream();
            var writer = new BinaryWriter(strm);

            writer.Write("RIFF".ToCharArray());
            writer.Write(bufferlength + 28);
            writer.Write("WAVEfmt ".ToCharArray());

            writer.Write(16);
            writer.Write((short)1);
            writer.Write((short)1);

            writer.Write(22050);
            writer.Write(22050);

            writer.Write((short)1);
            writer.Write((short)8);

            //data chunk
            writer.Write("data".ToCharArray());
            writer.Write(bufferlength);

            writer.Write(buffer, 0, bufferlength);

            return strm;
        }

        static void Main(string[] args)
        {
            string txt = null;
            foreach (var arg in args)
                txt += $"{arg} ";
            byte[] text = Encoding.ASCII.GetBytes(txt);
            Array.Resize(ref text, 256);
            TextToPhonemes(text);
            SetInput(text);
            SAMMain();
            var strm = Wave(GetBuffer(), GetBufferLength() / 50);
            strm.Position = 0;
            new SoundPlayer(strm).PlaySync();
        }
    }
}
